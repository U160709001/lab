public class FindPrimes {

    public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);
        StringBuilder result = new StringBuilder();

        for (int number = 2; number < max; number++) {
            boolean isPrime = true;
            for (int divisor = 2; divisor < number; divisor++) {
                if (number % divisor == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime)
                result.append(number).append(",");
        }

        System.out.println(result.toString().replaceAll(",$", ""));
    }
}