public class Rectangle {

    int sideA, sideB;
    Point topleft;

    public Rectangle(int sideA, int sideB, Point topleft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topleft = topleft;
    }

    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return 2 * (sideA + sideB);
    }

    public Point[] corners() {
        Point[] corners = new Point[4];
        corners[0] = topleft;
        Point topRight = new Point(topleft.xCoord + sideA, topleft.yCoord);
        Point bottomRight = new Point(topleft.xCoord + sideA, topleft.yCoord - sideB);
        Point bottomLeft = new Point(topleft.xCoord, topleft.yCoord - sideB);
        corners[1] = topRight;
        corners[2] = bottomLeft;
        corners[3] = bottomRight;
        return corners;
    }
}
