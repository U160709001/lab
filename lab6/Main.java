public class Main {

    public static void main(String[] args) {
        Point p = new Point(2, 5);
        Rectangle r = new Rectangle(6, 8, p);
        System.out.println("Rectangle Area: " + r.area());
        System.out.println("Rectangle Perimeter: " + r.perimeter());
        Point[] corners = r.corners();
        System.out.println("Rectangle Corners: ");
        for (Point corner : corners) {
            System.out.println("(" + corner.xCoord + ", " + corner.yCoord + ")" + " ");
        }

        Point p1 = new Point(7, -5);
        Circle c1 = new Circle(10, p1);
        System.out.println("Circle Area: " + c1.area());
        System.out.println("Circle Perimeter: " + c1.perimeter());

        Point p2 = new Point(10, 4);
        Circle c2 = new Circle(8, p2);
        System.out.println("Intersect: " + c1.intersect(c2));
    }
}
