public class Circle {

    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    public boolean intersect(Circle c) {
        double distanceBetweenCenters = Math.sqrt(
                Math.pow(c.center.xCoord - center.xCoord, 2) +
                        Math.pow(c.center.yCoord - center.yCoord, 2)
        );
        return distanceBetweenCenters < c.radius + radius;
    }
}
