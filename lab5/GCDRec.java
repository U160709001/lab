public class GCDRec {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        System.out.println(calculateGCD(Math.max(a, b), Math.min(a, b)));
    }

    private static int calculateGCD(int a, int b) {
        if (b == 0) {
            return a;
        }
        
        return calculateGCD(b, a % b);
    }
}