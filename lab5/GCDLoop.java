public class GCDLoop {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        System.out.println(calculateGCD(Math.max(a, b), Math.min(a, b)));
    }

    private static int calculateGCD(int a, int b) {
        int remainder;

        do {
            remainder = a % b;
            a = b;
            b = remainder;
        } while (remainder != 0);

        return a;
    }
}